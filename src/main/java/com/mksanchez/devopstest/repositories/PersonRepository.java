package com.mksanchez.devopstest.repositories;

import com.mksanchez.devopstest.model.Person;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(exported = false)
public interface PersonRepository extends PagingAndSortingRepository<Person, Long> {
}
